@extends('layouts.app')
@section('content')
    <div>
        <h1>Create news</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('news.store') }}">
            @csrf
            <div class="form-group">
                <label for="bodyNews"><b>Текст новости</b></label>
                <textarea class="form-control" name="body" id="bodyNews" cols="30" rows="10" placeholder="Введите новость..."></textarea>
                @error('body')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="category"><b>Select a category:</b></label>
                <select class="form-control border-success  @error('category_id') is-invalid border-danger @enderror"
                        id="category"  name="category_id">
                    <option value="" selected >'Choose'</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            @error('category_id')
            <p class="text-danger">{{ $message }}</p>
            @enderror
            <label for="tags_id"><b>Add tags</b></label>
            <div  id="tags">
                <div class="card card-body" style="margin: 5px">
                    @foreach($tags as $tag)
                        <div class="form-check">
                            <input class="form-check-input" name="tags[]" type="checkbox"
                                   value=" {{ $tag->id }} " id="tags{{$tag->id}}">
                            <label class="form-check-label" for="tags{{$tag->id}}">
                                {{$tag->tag}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
@endsection

