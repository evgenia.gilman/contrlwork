@extends('layouts.app')
@section('content')

    <h1>All news</h1>
        <div>
            <a href="{{route('news.create')}}" type="button" class="btn btn-sm btn-secondary" >Add news</a>
        </div>
    <div class="row">
        @if($news->count() > 0)
            @foreach($news as $one_news)
                    <div class="col-12">
                        <div class="card" style="margin: 10px">
                            <div class="card-body">
                                <p class="card-text">{{$one_news->body}}</p>
                                <h5 class="card-title">Author: {{$one_news->user->name}}</h5>
                                <p class="card-text"><small class="text-muted">Date of creation {{ $one_news->created_at->format('d M Y - H:i') }}</small></p>
                                <p class="card-text"><small class="text-muted">Date of publication {{ date($one_news->publication_date) }}</small></p>
                                <a href="{{route('news.edit', ['news' => $news])}}" class="card-link">Edit</a>
                                <form action="{{route('news.destroy',['news' => $news])}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-link" type="submit">Remove</button>
                                </form>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <div class="row justify-content-md-center p-5">
                <div class="col-md-auto">
                    {{$news->links('pagination::bootstrap-4') }}
                </div>
            </div>
        @else
            <p>No published news</p>
        @endif
    </div>

@endsection
