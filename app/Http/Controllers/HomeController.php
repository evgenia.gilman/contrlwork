<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(3);
        return view('home', compact('news'));
    }

}
