<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(3);
        return view('news.index', compact('news'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.create', compact('categories', 'tags'));
    }


    /**
     * @param NewsRequest $request
     * @return RedirectResponse
     */
    public function store(NewsRequest $request): RedirectResponse
    {
        $news = new News();
        $news->body = $request->input('body');
        $news->user()->associate($request->user());
        $news->category_id = $request->input('category_id');
        $news->publication_date = $request->input('publication_date');
        $news->quality = $request->input('quality');
        $news->relevance = $request->input('relevance');
        $news->satisfied = $request->input('satisfied');

        $news->save();
        return redirect()->route('news.index')->with('status', "Новость успешно создана!");
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function show(News $news)
    {
        $comments = $news->comments()->orderByDesc('updated_at')->paginate(5);
        return view('news.show', compact('news', 'comments'));
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function edit(News $news)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.edit', compact('news', 'categories', 'tags'));
    }


    /**
     * @param NewsRequest $request
     * @param News $news
     * @return RedirectResponse
     */
    public function update(NewsRequest $request, News $news): RedirectResponse
    {
        $data = $request->all();
        $news->update($data);

        return redirect()->route('news.index')->with('status', 'Новость успешно обновлена!');
    }


    /**
     * @param News $news
     * @return RedirectResponse
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect()->route('news.index')->with('status', 'Новость успешно удалена!');
    }
}
