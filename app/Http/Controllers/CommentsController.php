<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\News;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @param News $news
     * @param CommentRequest $request
     * @return RedirectResponse
     */
    public function store(News $news, CommentRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $data['news_id'] = $news->id;

        Comment::create($data);

        return redirect()->route('news.show', $news->id);
    }


    public function update(News $news, Comment $comment, CommentRequest $request): RedirectResponse
    {
        $this->authorize('update', $comment);
        $data = $request->validated();
        $comment->update($data);

        return redirect()->back();
    }


    public function destroy(News $news, Comment $comment): RedirectResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->back();
    }
}
