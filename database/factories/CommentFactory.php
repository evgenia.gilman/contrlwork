<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->realText(rand(11, 30)),
            'user_id' => rand(1,20),
            'news_id' => rand(1, 50),
            'confirm' => rand(0,1)
        ];
    }
}
