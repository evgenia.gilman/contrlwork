<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->paragraph(30),
            'user_id' => rand(1, 20),
            'category_id' => rand(1, 10),
            'publication_date' => $this->faker->date,
            'quality' => rand(1, -1),
            'relevance' => rand(1, -1),
            'satisfied' => rand(1, -1)
        ];
    }
}
