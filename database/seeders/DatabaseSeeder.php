<?php

namespace Database\Seeders;

use App\Models\News;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(CommentTableSeeder::class);

        $news = News::all();
        $tags = Tag::all();


        for ($i = 0; $i < 10; $i++) {
            $tags->each(function ($tag) use ($news) {
                $tag->news()->attach(
                    $news->random(1)->pluck('id')->toArray()
                );
            });
        }
    }
}
