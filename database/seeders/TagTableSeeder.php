<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            ['tag' => 'politicsNews'],
            ['tag' => 'economicNews'],
            ['tag' => 'scienceNews'],
            ['tag' => 'cultureNews'],
            ['tag' => 'sportsNews'],
            ['tag' => 'healthNews'],
        ];

        foreach ($tags as $key => $value) {
            Tag::create($value);
        }
    }
}
